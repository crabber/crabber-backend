(function() {
	'use strict';
    
    var log = require('loglevel');

	// Setup default environment variables
	process.env.NODE_ENV = process.env.NODE_ENV || 'production';
	process.env.PORT = process.env.PORT || 5001;
	
	// Ensure real secrets are used for cryptography in production mode
	if(process.env.NODE_ENV === 'production') {
		
		['COOKIE_SECRET', 'JWT_PUBLIC_KEY', 'JWT_PRIVATE_KEY'].forEach(function(val) {
			
			if(process.env[val] === undefined) {
				log.error('Starting in production mode but environment variable %s required for strong cryptography is not set!', val);
				process.exit(1);
			}
		});
	}
    
    if(process.env.NODE_ENV === 'production') {
        log.setLevel(log.levels.INFO);
    } else {
        log.setLevel(log.levels.DEBUG);   
    }
	
	// Load and initialize express and socket.io
	var app = require('express')(),
		server = require('http').createServer(app),
                io = require('socket.io')(server);
                
                
        // Socket.io
        io.sockets.on('connection', function(socket) {
          log.debug('client connected!');
        });
        
	// bluebird integration
	var Promise = require('bluebird');
	Promise.longStackTraces();
	Promise.promisifyAll(require("joi"));
	Promise.promisifyAll(require("mongoose"));
        
	// Express settings
	require('./config/express')(app);

	// Database
	require('./config/db');


	// Start server
	if (process.env.NODE_ENV !== 'test') {
		server.listen(process.env.PORT, function() {
			log.info('Server started on port %d in %s mode! Waiting for requests...', process.env.PORT, process.env.NODE_ENV);
		});
	}

	module.exports.app = app;
	module.exports.server = server;
})();
