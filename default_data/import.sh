#! /bin/sh

mongoimport --db crabber --collection groups --type json --jsonArray --file groups.json
mongoimport --db crabber --collection users --type json --jsonArray --file users.json
mongoimport --db crabber --collection tagsharemodificators --type json --jsonArray --file tagShareModificators.json
