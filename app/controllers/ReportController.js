(function() {
	'use strict';

	var Group = require('../models/Group');
    var Promise = require('bluebird');
	var bcrypt = require('bcrypt');
    var bookingController = require('./BookingController');
    var userController = require('./UserController');
    var tagTagModificatorController = require('./TagModificatorController');
    var log = require('loglevel');

	function ReportController() {
	}
	
	
	ReportController.prototype.expensesForGroup = function(groupId, creditorId, range, tags) {
		var promise = bookingController.getBookingsForGroup(groupId, creditorId, undefined, range, tags);

		promise = promise.then(function(bookingsInGroup) {
		
			return calculateExpenses(bookingsInGroup, creditorId);
		});
		
		return promise;
	};
	
	ReportController.prototype.shareForGroup = function(groupId, debtorId, range, tags) {
	
		var promise = bookingController.getBookingsForGroup(groupId, undefined, debtorId, range, tags);

		promise = promise.then(function(bookingsInGroup) {
		
			return calculateShare(bookingsInGroup, debtorId);
		});
		
		return promise;
	};
	
	ReportController.prototype.balanceForGroup = function(groupId, userId, range, tags) {
		
		var promises = [this.expensesForGroup(groupId, userId, range, tags), this.shareForGroup(groupId, userId, range, tags)];
		
		return Promise.settle(promises).then(function(results) {
			
			if(!results[0].isFulfilled() || !results[1].isFulfilled()) {
			
				log.error('Some promises were not fulfilled');
				return;
			}
		
			var expenses = results[0].value().expenses;
			var debts = results[1].value().share;
			
			log.debug('Expenses: %s', JSON.stringify(expenses));
			log.debug('Debts: %s', JSON.stringify(debts));
			log.debug('Balance: %s', expenses-debts);
			
			return {balance: expenses-debts};
		});
	};

	function calculateExpenses(bookingsInGroup, creditorId) {
		
		return new Promise(function(resolve, reject) {
		
			var expenses = bookingsInGroup.reduce(function(previousValue, booking) {
						
				var totalBookingAmount = booking.creditors.reduce(function(previousLocalValue, creditor) {
					
					if(!creditorId || creditor.by.equals(creditorId)) {
						return previousLocalValue + creditor.amount.value;
					}
					return previousLocalValue;
						
				}, 0);
				
				return previousValue + totalBookingAmount;
			}, 0);
				
			resolve({expenses: expenses});
		});
	}
	
	function calculateShare(bookingsInGroup, debtorId) {
		
		var sharePerBookingPromises = bookingsInGroup.map(function(booking) {
		
			return tagTagModificatorController.applyShareModificators(booking, debtorId);
		});
		
		var promise = Promise.settle(sharePerBookingPromises).then(function(sharesPerBooking) {
			
			var shareSum = 0;
		
			sharesPerBooking.forEach(function(sharePerBooking) {
			
				if(!sharePerBooking.isFulfilled()) {
					
					log.error('sharePerBooking promise was not fulfilled');
					return;
				}
				log.debug('sharePerBooking: %s', JSON.stringify(sharePerBooking.value()));
				shareSum += sharePerBooking.value();
			});
			
			log.debug('shareSum: %s', shareSum);
			
			return { share: shareSum };
		});
		
		return promise;
	}
	
	module.exports = new ReportController();
})();
