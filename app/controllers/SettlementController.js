/*jslint node: true */
(function() {
    'use strict';

    var Settlement = require('../models/Settlement');
    var Promise = require('bluebird');
    var then = Promise.try;
    var bookingController = require('./BookingController');
    var log = require('loglevel');

    function SettlementController() {

    }

    /**
	 * Return a promise which lists all settlements and applys optional filter.
	 *
	 * @param filter
	 * @return Promise
	 */
    SettlementController.prototype.find = function(filter) {
        return Settlement.find(filter).populate('balances.debtor balances.creditor').execAsync();
    };

    /**
	 * Return a promise which create a new settlement.
	 *
	 * @param creator user who is creating the settlement
	 * @param settlement
	 * @return Promise
	 */
    SettlementController.prototype.create = function(creator, settlement) {

        var self = this;
        var promise;

        promise = Promise.try(function() {
            settlement = new Settlement(settlement);

            settlement.changelog = [{
                timestamp: Date.now(),
                by: creator._id,
                event: 'created',
                description: 'Created'
            }];

            log.debug(settlement);

            return settlement;
        });

        promise = promise.then(function(settlement) {
            return settlement.saveAsync();
        });

        promise = promise.tap(function(settlement) {
            return self.clearBookings(settlement);
        });

        return promise;
    };

    SettlementController.prototype.getAllRelevantBookings = function(settlement) {

        return Promise.try(function() {
            // Get bookings where
            // the settlement creditor is participating as creditor and
            // the settlement debtor is participating as debtor.
            var bookings = settlement.balances.map(function(balance) {
                return bookingController.getBookingsForGroup(settlement.group,
                                                             balance.creditor,
                                                             balance.debtor,
                                                             {start: settlement.interval_start,
                                                              end: settlement.interval_end});
            });

            // Other direction:
            // Get bookings where
            // the settlement creditor is participating as debotr and
            // the settlement debtor is participating as creditor.
            bookings = bookings.concat(settlement.balances.map(function(balance) {
                return bookingController.getBookingsForGroup(settlement.group,
                                                             balance.debtor,
                                                             balance.creditor,
                                                             {start: settlement.interval_start,
                                                              end: settlement.interval_end});
            }));

            return bookings;
        });
    };

    SettlementController.prototype.clearBookings = function(settlement) {

        var promise;
        var self = this;

        promise = Promise.all(self.getAllRelevantBookings(settlement));

        promise = promise.then(function(balancesBookings) {

            return Promise.all(balancesBookings.map(function(bookings) {
                return Promise.all(bookings.map(function(booking) {

                    if(!booking.cleared_by) {
                        booking.cleared_by = settlement._id;
                        return booking.saveAsync();
                    }
                    return booking;
                }));
            }));
        });

        return promise;
    };

    module.exports = new SettlementController();

})();
