(function() {
	'use strict';

	var TagShareModificator = require('../models/TagShareModificator');
    var Promise = require('bluebird');
    var bcrypt = require('bcrypt');
    var userController = require('./UserController');
    var log = require('loglevel');

	function TagModificatorController() {
		
	}
	
	/**
	 * Return a promise which list all tag share modificators and applies optional filter.
	 *
	 * @param filter
	 * @return Promise
	 */
	TagModificatorController.prototype.findShareModificators = function(filter) {
		return TagShareModificator.find(filter).select('_id name description').execAsync();
	};

	

	TagModificatorController.prototype.applyShareModificators = function(booking, debtorId) {
		
		var promise = TagShareModificator.find().where('tags').in(booking.tags).execAsync();
		
		promise = promise.then(function(tagShareModificators) {
			
			return new Promise(function (resolve, reject) {
				
				userController.getUsersInGroup(booking.group).then(function(usersInGroup) {
				
					resolve([usersInGroup, tagShareModificators]);
				});
			});
		});
		
		promise = promise.then(function(results) {
			
			return new Promise(function(resolve, reject) {
				var usersInGroup = results[0];
				var tagShareModificators = results[1];
				
				var totalBookingAmount = booking.creditors.reduce(function(previousValue, creditor) {
					return previousValue + creditor.amount.value;
				}, 0);
				
				log.debug('Total booking amount: ', totalBookingAmount);
				
				var totalShares = booking.debtors.length;
					
				var userShares = {};
				
				usersInGroup.forEach(function(userInGroup) {
					
					userShares[userInGroup._id.toString()] = 1;
				});
				
				tagShareModificators.forEach(function(tagShareModificator) {
					
					totalShares += tagShareModificator.totalSharesIncrease;
					tagShareModificator.userSharesIncreases.forEach(function(userSharesIncrease) {
					
						userShares[userSharesIncrease.user] += userSharesIncrease.increase;
					});
				});
				
				log.debug('total shares: ', totalShares);
				log.debug('user shares: ', JSON.stringify(userShares));
				log.debug('debtor id: %s', debtorId);
				log.debug('user share abs: ', JSON.stringify(totalBookingAmount * (userShares[debtorId] / totalShares)));
				
				resolve(totalBookingAmount * (userShares[debtorId] / totalShares));
			});
		}).catch(function (error) {
		
			log.error(error);
		});
		
		return promise;
	};

	module.exports = new TagModificatorController();
})();
