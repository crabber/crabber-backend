/*jslint node: true */
'use strict';

const Booking = require('../models/Booking');
const Promise = require('bluebird');
const emailController = require('./EmailController');
const log = require('loglevel');
const errors = require('../errors');
const ObjectId = require('mongoose').Types.ObjectId;

function BookingController() {

}

/**
* Return a promise which lists all bookings and applys optional filter.
*
* @param filter
* @return Promise
*/
BookingController.prototype.find = function(filter) {
	//return Booking.find({cleared_by: { $exists: false }}).populate("debtors creditors.by").select('_id booking_date description amount creditors debtors tags').execAsync();
	return Booking.find().populate("debtors creditors.by").execAsync();
};

/**
* Return a promise which create a new booking.
*
* @param creator user who is creating the booking
* @param booking
* @return Promise
*/
BookingController.prototype.create = function(creator, booking) {

	const self = this;
	booking = new Booking(booking);
	log.debug(booking);
	booking.created.by = creator._id;

	var promise = booking.saveAsync();

	promise = promise.then(function(booking) {

		console.log(booking);

		return self.findById(booking._id);
	});

	promise = promise.tap(function(booking) {
		return emailController.sendBookingCreatedNotificationEmail(booking);
	});

	return promise;
};

/**
* Return a promise which find one booking by id.
*
* @param id
* @return Promise
*/
BookingController.prototype.findById = function(id) {
	return Booking.findOne({'_id': id}).populate('debtors creditors.by created.by').execAsync().then(function(booking) {
		if (!booking) {
			throw new errors.ResourceNotFoundError('Booking', id);
		}
		return booking;
	});
};

BookingController.prototype.getBookingsForGroup = function(groupId, creditorId, debtorId, range, tags) {

	var query = { group: groupId };

	if(creditorId) {
		query['creditors.by'] = creditorId;
	}

	if(debtorId) {
		query.debtors = debtorId;
	}

	if(range) {
		query.booking_date = {"$gte": new Date(range.start), "$lt": new Date(range.end) };
	}

	if(tags) {
		query.tags = { $in: tags };
	}

	return Booking.find(query).select('_id booking_date description amount creditors debtors tags group').execAsync();
};

/**
* Return a promise which updates a booking.
*
* @param udpater user who is performing the update
* @param id booking id to update
* @param booking new booking
* @return Promise
*/
BookingController.prototype.update = function(updater, id, newBooking) {

	var self = this;
	var promise = this.findById(id).then(function(booking) {

		// FIXME solve more elegantly
		['booking_date', 'description', 'tags', 'creditors', 'debtors'].forEach(function(field) {

			if(newBooking[field] !== undefined) {
				booking[field] = newBooking[field];
			}
		});

		booking.creditors = newBooking.creditors;

		booking.updated.timestamp = Date.now();
		booking.updated.by = updater._id;

		return booking;
	});

	promise = promise.tap(function(booking) {
		return booking.saveAsync();
	});

	promise = promise.then(function(booking) {
		return self.findById(booking._id);
	});

	promise = promise.tap(function(booking) {
		return emailController.sendBookingModifiedNotificationEmail(booking);
	});

	return promise;
};


module.exports = new BookingController();
