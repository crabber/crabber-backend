
(function() {
	'use strict';

	var Group = require('../models/Group');
    var User = require('../models/User');
    var Promise = require('bluebird');
    var bcrypt = require('bcrypt');
    var then = Promise.try;

	function GroupController() {
	}

	/**
	 * Return a promise which list all groups and apply optional filter.
	 *
	 * @param filter
	 * @return Promise
	 */
	GroupController.prototype.find = function(filter) {
		return Group.find(filter).select('_id name description').execAsync();
	};

	/**
	 * Return a promise which find one group by id.
	 *
	 * @param id
	 * @return Promise
	 */
	GroupController.prototype.findById = function(id) {
		return Group.findByIdAsync(id).then(function(group) {
			if (!group) {
				//throw new errors.ResourceNotFoundError('User', id);
			}
			return group;
		});
	};
	
	/**
	 * Return a promise which create a new group.
	 *
	 * @param group
	 * @return Promise
	 */
	GroupController.prototype.create = function(creator, group) {
		
		var promise = then(function(group) {
		
			group = new Group(group);
			group.created.by = creator._id;
			return Promise.promisify(group.save, group)();
		});
		
		promise = promise.get(0).then(function(group) {
			
			return group;
		});
		
		return promise;
	};

	/**
	 * Return a promise which updates the group.
	 *
	 * @param id
	 * @param group new group
	 * @return Promise
	 */
	GroupController.prototype.update = function(updater, id, newGroup) {
		return this.findById(id).tap(function(group) {

                      // FIXME solve more elegantly
			['name', 'description'].forEach(function(field) {
                          
                          if(newGroup[field] !== undefined) {
                            group[field] = newGroup[field];
                          }
                        });
			
			group.updated.timestamp = Date.now();
			group.updated.by = updater._id;

			return group;
		}).tap(function(group) {
			return group.saveAsync();//.thenReturn(user);
		});
	};
    
    /**
	 * Return a promise which returns all users of a group
	 *
	 * @param group_id
	 * @return Promise
	 */
	GroupController.prototype.getUsersInGroup = function(group_id) {
        
        return User.find({groups: { $in: [group_id]}}).execAsync();
	};

	/**
	 * Return a promise which removes a group.
	 *
	 * @param id
	 * @return Promise
	 */
	GroupController.prototype.removeById = function(remover, id) {

	};

	module.exports = new GroupController();

})();
