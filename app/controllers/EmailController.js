(function() {
    'use strict';

    var Promise = require('bluebird');
    var bcrypt = require('bcrypt');
    var userController = require('./UserController');
    var groupController = require('./GroupController');
    var log = require('loglevel');
    var nodemailer = require('nodemailer');
    var smtpTransport = require('nodemailer-smtp-transport');

    function EmailController() {

        var options = {
            host: 'mail.messagingengine.com',
            port: 465,
            secure: true,  
            auth: {
                user: process.env.SMTP_USER,
                pass: process.env.SMTP_PASSWORD
            },
            ignoreTLS: true
        };

        this.transporter = nodemailer.createTransport(smtpTransport(options));
    }
    
    EmailController.prototype.sendBookingCreatedNotificationEmail = function(booking) {
        
        return this.sendBookingNotificationEmail(booking,
                                                 'Neue Buchung von ' + booking.created.by.display_name +'!',
                                                booking.created.by.display_name + ' hat eine neue Buchung angelegt:');
    };
    
    EmailController.prototype.sendBookingModifiedNotificationEmail = function(booking) {
        
        return this.sendBookingNotificationEmail(booking,
                                                 booking.created.by.display_name +' hat eine Buchung modifiziert!',
                                                booking.created.by.display_name + ' hat eine Buchung bearbeitet. Die geänderte Version ist folgende:');
    };

    /**
	 * Return a promise which sends an email to notify other users in the group
     * about a newly created booking
	 *
	 * @param booking
	 * @return Promise
	 */
    EmailController.prototype.sendBookingNotificationEmail = function(booking, subject, message) {
        
        var self = this;
        
        var promise = groupController.getUsersInGroup(booking.group).then(function(usersInGroup) {

            return new Promise(function(resolve, reject) {
                
                var receiverList = usersInGroup.reduce(function(list, user) {
                    list = list ? list + ', ' : '';
                    
                    return list += user.display_name + '<' + user.email +'>';                    
                }, null);

                var tagsText = booking.tags.reduce(function(text, tag) {

                    text = text ? text + ', ' : '';
                    return text += tag;
                }, null);

                var creditorsText = booking.creditors.reduce(function(text, creditor) {
                    text = text ? text + ', ' : '';

                    return text += ((creditor.amount.value / 100).toString().replace('.', ',')) + ' EUR von ' + creditor.by.display_name;
                }, null);

                var debtorsText = booking.debtors.reduce(function(text, debtor) {
                    text = text ? text + ', ' : '';

                    return text += debtor.display_name;
                }, null);

                var mailOptions = {
                    from: 'Crabber <crabber@cubie.cute.computer>',
                    to: receiverList,
                    subject: subject,
                    text: message + '\n\n' +
                    'Beschreibung: ' + booking.description + '\n' +
                    'Tags: ' + tagsText + '\n' + 
                    'Geldgeber: ' +  creditorsText + '\n' +
                    'Beteiligte: ' + debtorsText
                };
                
                log.debug('Sending email to ' + receiverList);

                self.transporter.sendMail(mailOptions, function(error, info){
                    if(error){
                        reject(error);
                    }else{
                        resolve(info.response);
                    }
                });

            });
        });

        return promise;
    };


    module.exports = new EmailController();
})();
