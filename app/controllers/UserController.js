
(function() {
	'use strict';

	var User = require('../models/User');
    var Promise = require('bluebird');
    var bcrypt = require('bcrypt');
    var ObjectID = require('mongodb').ObjectID;
    var log = require('loglevel');

	function UserController() {
	}

	/**
	 * Return a promise which list all users and apply optional filter.
	 *
	 * @param filter
	 * @return Promise
	 */
	UserController.prototype.find = function(filter) {
		return User.find(filter).select('_id email full_name display_name').execAsync();
	};

	/**
	 * Return a promise which find one user by id.
	 *
	 * @param id
	 * @return Promise
	 */
	UserController.prototype.findById = function(id) {
		return User.findByIdAsync(id).then(function(user) {
			if (!user) {
				//throw new errors.ResourceNotFoundError('User', id);
			}
			return user;
		});
	};
	
	/**
	 * Return a promise which find one user by its email
	 *
	 * @param email
	 * @return Promise
	 */
	UserController.prototype.findByEmail = function(email) {
		return User.findOneAsync({email: email}).then(function(user) {
			if (!user) {
				//throw new errors.ResourceNotFoundError('User', email);
			}
			return user;
		});
	};
	
	UserController.prototype.getUsersInGroup = function(groupId) {

		return User.find({'groups': groupId}).execAsync();
	};
	
	/**
	 * Return a promise which create a new user.
	 *
	 * @param user
	 * @return Promise
	 */
	UserController.prototype.create = function(creator, user) {
		
		// hash password
		var promise = Promise.promisify(bcrypt.hash)(user.password, 10);

		promise = promise.then(function(hash) {
		
			delete user.password;
			user.password_hash = hash;
			return user;
		});
		
		promise = promise.then(function(user) {
		
			user = new User(user);
			user.created.by = creator._id;
			return Promise.promisify(user.save, user)();
		});
		
		promise = promise.get(0).then(function(user) {
			
			return user;
		});
		
		return promise;
	};

	/**
	 * Return a promise which updates the user.
	 *
	 * @param id
	 * @param user new user
	 * @return Promise
	 */
	UserController.prototype.update = function(updater, id, newUser) {
		return this.findById(id).tap(function(user) {

                      // FIXME solve more elegantly
			['login', 'password_hash', 'display_name', 'full_name'].forEach(function(field) {
                          
                          if(newUser[field] !== undefined) {
                            user[field] = newUser[field];
                          }
                        });
			
			user.updated.timestamp = Date.now();
			user.updated.by = updater._id;

			//return validate.user(user._doc);//.thenReturn(user);
			return true;
		}).tap(function(user) {
			return user.saveAsync();//.thenReturn(user);
		});
	};

	/**
	 * Return a promise which removes a user.
	 *
	 * @param id
	 * @return Promise
	 */
	UserController.prototype.removeById = function(remover, id) {
// 		return this.findById(id).then(function(user) {
// 
// 			log.debug(arguments);
// 			user.deleted.by = remover._id;
// 
// 			if (user.status === 'DRAFT') {
// 				return user.removeAsync();
// 			} else {
// 				//throw new errors.AuthorizationError('Could not remove user with status ' + user.status);
// 			}
// 		});
	};

	module.exports = new UserController();

})();
