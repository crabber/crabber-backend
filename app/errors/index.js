
function AuthorizationError(message) {
	this.statusCode = 403;
	this.name = 'AuthorizationError';
	this.message = message;
	Error.captureStackTrace(this, AuthorizationError);
}

AuthorizationError.prototype = Object.create(Error.prototype);
AuthorizationError.prototype.constructor = AuthorizationError;

exports.AuthorizationError = AuthorizationError;


function ResourceNotFoundError(resource, id) {
	this.statusCode = 404;
	this.name = 'ResourceNotFoundError';
	this.message = resource + ' (' + id + ')' + ' not found!';
	Error.captureStackTrace(this, ResourceNotFoundError);
}

ResourceNotFoundError.prototype = Object.create(Error.prototype);
ResourceNotFoundError.prototype.constructor = ResourceNotFoundError;

exports.ResourceNotFoundError = ResourceNotFoundError;