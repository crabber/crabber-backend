
(function() {
    'use strict';

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;
    var changelogSubschema = require('./ChangelogSubschema');

    var schema = new Schema({
        group: {
            type: Schema.Types.ObjectId,
            ref: 'Group',
            required: true
        },
        booking_date: {
            type: Date,
            required: true,
            default: Date.now
        },
        interval_start: {
            type: Date,
            required: true,
            default: Date.now
        },
        interval_end: {
            type: Date,
            required: true,
            default: Date.now
        },
        balances: [{
            creditor: {
                type: Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            debtor: {
                type: Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            amount: { 
                currency: {
                    type: String, enum: [ 'EUR' ],
                    required: true,
                    default: 'EUR'
                },
                value: {
                    type: Number,
                    required: true
                }
            },
        }],
        changelog: [ changelogSubschema ]
    });

    module.exports = mongoose.model('Settlement', schema);
})();
