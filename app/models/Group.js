
(function() {
	'use strict';

	var mongoose = require('mongoose'),
		Schema = mongoose.Schema;

	var schema = new Schema({
		created: {
			timestamp: { 
				type: Date, 
				default: Date.now,
				required: true
			},
			by: {
				type: Schema.Types.ObjectId,
				ref: 'User'
			}
		},
		updated: {
			timestamp: { 
				type: Date,
				required: false
			},
			by: {
				type: Schema.Types.ObjectId,
				ref: 'User'
			}
		},
		deleted: {
			timestamp: {
				type: Date,
				required: false
			},
			by: {
				type: Schema.Types.ObjectId,
				ref: 'User'
			}
		},
                name: {
			type: String,
			required: true
		},
                description: {
			type: String,
			required: true
		}
	});

	module.exports = mongoose.model('Group', schema);

})();
