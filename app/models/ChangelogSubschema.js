(function () {
    'use strict';
    
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var changelogSubschema = {

        timestamp: { type: Date, 'default': Date.now, required: true },
        event: { type: String, required: true, 'enum': [ 'created', 'updated', 'deactivated', 'activated' ] },
        by: { type: Schema.Types.ObjectId, ref: 'User' },
        description: { type: String, required: true }
    };

    module.exports = changelogSubschema;
})();
