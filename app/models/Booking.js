
/*jslint node: true */
'use strict';

const mongoose = require('mongoose');
const Promise = require('bluebird');
const Schema = mongoose.Schema;

const schema = new Schema({
  created: {
    timestamp: {
      type: Date,
      default: Date.now,
      required: true
    },
    by: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  updated: {
    timestamp: {
      type: Date,
      required: false
    },
    by: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  deleted: {
    timestamp: {
      type: Date,
      required: false
    },
    by: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  group: {
    type: Schema.Types.ObjectId,
    ref: 'Group',
    required: true
  },
  booking_date: {
    type: Date,
    required: true,
    default: Date.now
  },
  description: {
    type: String,
    required: true
  },
  tags: [{
    type: String
  }],
  creditors: [{
    amount: {
      currency: {
        type: String, enum: [ 'EUR' ],
        required: true,
        default: 'EUR'
      },
      value: {
        type: Number,
        required: true
      }
    },
    by: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    }
  }],
  debtors: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  cleared_by: {
    type: Schema.Types.ObjectId,
    ref: 'Settlement'
  }
});

const model = mongoose.model('Booking', schema);

Promise.promisifyAll(model);

module.exports = model;
