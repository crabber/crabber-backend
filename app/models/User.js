
(function() {
    'use strict';

    var mongoose = require('mongoose'),
        Schema = mongoose.Schema;

    var schema = new Schema({
        created: {
            timestamp: { 
                type: Date, 
                default: Date.now,
                required: true
            },
            by: {
                type: Schema.Types.ObjectId,
                ref: 'User'
            }
        },
        updated: {
            timestamp: { 
                type: Date,
                required: false
            },
            by: {
                type: Schema.Types.ObjectId,
                ref: 'User'
            }
        },
        deleted: {
            timestamp: {
                type: Date,
                required: false
            },
            by: {
                type: Schema.Types.ObjectId,
                ref: 'User'
            }
        },
        email: {
            type: String,
            required: true,
            index: {
                unique: true
            }
        },
        password_hash: {
            type: String,
            required: true
        },
        display_name:{
            type: String,
            required: false
        },
        full_name: {
            type: String,
            required: true
        },
        roles: [{
            type: String,
            enum: [ 'user', 'sysadmin' ]
        }],
        groups: [{
            type: Schema.Types.ObjectId,
            ref: 'Group'
        }]
    });

    module.exports = mongoose.model('User', schema);

})();
