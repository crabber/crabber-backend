'use strict';

var express = require('express'),
	reports = express.Router(),
	Joi = require('joi'),
	reportController = require('../controllers/ReportController');

var send = function(status, res) {
	if (!res) {
		res = status;
		status = null;
	}

	return function(data) {
		if (status) {
			res.status(status);
		}
		res.send(data);
	};
};

reports.get('/expenses', function(req, res, next) {
	
	var range;
	
	if(req.query.start && req.query.end) {
		range = { start: req.query.start, end: req.query.end };
	}
	
	var tags = req.query.tags ? req.query.tags.split(',') : undefined;
	
	
	reportController.expensesForGroup(req.query.groupId, req.query.userId, range, tags).then(send(res)).catch(next);
});

reports.get('/share', function(req, res, next) {
	
	var range;
	
	if(req.query.start && req.query.end) {
		range = { start: req.query.start, end: req.query.end };
	}
	
	var tags = req.query.tags ? req.query.tags.split(',') : undefined;
	
	reportController.shareForGroup(req.query.groupId, req.query.userId, range, tags).then(send(res)).catch(next);
});

reports.get('/balance', function(req, res, next) {
	
	var range;
	
	if(req.query.start && req.query.end) {
		range = { start: req.query.start, end: req.query.end };
	}
	
	var tags = req.query.tags ? req.query.tags.split(',') : undefined;
	
	reportController.balanceForGroup(req.query.groupId, req.query.userId, range, tags).then(send(res)).catch(next);
});

module.exports = reports;
