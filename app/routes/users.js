'use strict';

var express = require('express'),
	users = express.Router(),
	Joi = require('joi'),
	userController = require('../controllers/UserController');

var send = function(status, res) {
	if (!res) {
		res = status;
		status = null;
	}

	return function(data) {
		if (status) {
			res.status(status);
		}
		res.send(data);
	};
};

users.get('/', function(req, res, next) {
	userController.find(req.query).then(send(res)).catch(next);
});

users.get('/:id', function(req, res, next) {
	userController.findById(req.params.id).then(send(res)).catch(next);
});

users.post('/', function(req, res, next) {
	userController.create(req.user, req.body).then(send(res)).catch(next);
});

users.put('/:id', function(req, res, next) {
	userController.update(req.user, req.params.id, req.body).then(send(res)).catch(next);
});

users.delete('/:id', function(req, res, next) {
	userController.removeById(req.user, req.params.id, req.body).then(send(204, res)).catch(next);
});

module.exports = users;
