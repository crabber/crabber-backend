'use strict';

var express = require('express'),
	bookings = express.Router(),
	Joi = require('joi'),
	bookingController = require('../controllers/BookingController');

var send = function(status, res) {
	if (!res) {
		res = status;
		status = null;
	}

	return function(data) {
		if (status) {
			res.status(status);
		}
		res.send(data);
	};
};

bookings.get('/', function(req, res, next) {
	bookingController.find(req.query).then(send(res)).catch(next);
});

bookings.get('/:id', function(req, res, next) {
	bookingController.findById(req.params.id).then(send(res)).catch(next);
});

bookings.post('/', function(req, res, next) {
	bookingController.create(req.user, req.body).then(send(res)).catch(next);
});

bookings.put('/:id', function(req, res, next) {
	bookingController.update(req.user, req.params.id, req.body).then(send(res)).catch(next);
});

bookings.delete('/:id', function(req, res, next) {
	bookingController.removeById(req.user, req.params.id, req.body).then(send(204, res)).catch(next);
});

module.exports = bookings;
