'use strict';

var express = require('express'),
	bookings = express.Router(),
	Joi = require('joi'),
	settlementController = require('../controllers/SettlementController');

var send = function(status, res) {
	if (!res) {
		res = status;
		status = null;
	}

	return function(data) {
		if (status) {
			res.status(status);
		}
		res.send(data);
	};
};

bookings.get('/', function(req, res, next) {
	settlementController.find(req.query).then(send(res)).catch(next);
});

bookings.get('/:id', function(req, res, next) {
	settlementController.findById(req.params.id).then(send(res)).catch(next);
});

bookings.post('/', function(req, res, next) {
	settlementController.create(req.user, req.body).then(send(res)).catch(next);
});

bookings.put('/:id', function(req, res, next) {
	settlementController.update(req.user, req.params.id, req.body).then(send(res)).catch(next);
});

bookings.delete('/:id', function(req, res, next) {
	settlementController.removeById(req.user, req.params.id, req.body).then(send(204, res)).catch(next);
});

module.exports = bookings;
