'use strict';

var express = require('express'),
	mongoose = require('mongoose'),
	routes = express.Router();

routes.get('/', function(req, res) {
	res.status(200).send('Hello! :)');
});

routes.use('/bookings', require('./bookings'));
routes.use('/users', require('./users'));
routes.use('/groups', require('./groups'));
routes.use('/tagShareModificators', require('./tagShareModificators'));
routes.use('/reports', require('./reports'));
routes.use('/settlements', require('./settlements'));

/**
 * Handle common errors...
 */
routes.use(function(err, req, res, next) {
	if (err && err.statusCode) {
		return res.status(err.statusCode).send({ name: err.name, message: err.message });
	}


	if (err && err.name === 'CastError' && err.type === 'ObjectId') {
		res.status(404).send({ name: 'NotFound', message: 'Wrong ObjectId format' });
	} else if (err && err.name === 'OperationalError' && err.cause && err.cause.name === 'ValidationError') {
		res.status(400).send({ name: 'ValidationError', message: 'JOI validation error', detail: err.cause.details, _object: err.cause._object });
	} else if (err && err.name === 'ValidationError' && err.errors) {
		res.status(400).send({ name: 'ValidationError', message: 'Mongoose validation error', detail: err.errors });
	} else {
		next(err);
	}
});

module.exports = routes;
