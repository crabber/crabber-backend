'use strict';

var express = require('express'),
	tagShareModificators = express.Router(),
	Joi = require('joi'),
	tagModificatorController = require('../controllers/TagModificatorController');

var send = function(status, res) {
	if (!res) {
		res = status;
		status = null;
	}

	return function(data) {
		if (status) {
			res.status(status);
		}
		res.send(data);
	};
};

tagShareModificators.get('/', function(req, res, next) {
	tagModificatorController.findShareModificators(req.query).then(send(res)).catch(next);
});

tagShareModificators.get('/:id', function(req, res, next) {
	tagModificatorController.findById(req.params.id).then(send(res)).catch(next);
});

tagShareModificators.post('/', function(req, res, next) {
	tagModificatorController.create(req.user, req.body).then(send(res)).catch(next);
});

tagShareModificators.put('/:id', function(req, res, next) {
	tagModificatorController.update(req.user, req.params.id, req.body).then(send(res)).catch(next);
});

tagShareModificators.delete('/:id', function(req, res, next) {
	tagModificatorController.removeById(req.user, req.params.id, req.body).then(send(204, res)).catch(next);
});

module.exports = tagShareModificators;
