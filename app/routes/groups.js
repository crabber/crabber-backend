'use strict';

var express = require('express'),
	groups = express.Router(),
	Joi = require('joi'),
	groupController = require('../controllers/GroupController');

var send = function(status, res) {
	if (!res) {
		res = status;
		status = null;
	}

	return function(data) {
		if (status) {
			res.status(status);
		}
		res.send(data);
	};
};

groups.get('/', function(req, res, next) {
	groupController.find(req.query).then(send(res)).catch(next);
});

groups.get('/:id', function(req, res, next) {
	groupController.findById(req.params.id).then(send(res)).catch(next);
});

groups.post('/', function(req, res, next) {
	groupController.create(req.user, req.body).then(send(res)).catch(next);
});

groups.put('/:id', function(req, res, next) {
	groupController.update(req.user, req.params.id, req.body).then(send(res)).catch(next);
});

groups.delete('/:id', function(req, res, next) {
	groupController.removeById(req.user, req.params.id, req.body).then(send(204, res)).catch(next);
});

module.exports = groups;
