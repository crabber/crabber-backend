FROM node:4
MAINTAINER Fabian Köster <koesterreich@fastmail.fm>

# Set timezone to Europe/Berlin
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Set workdir
WORKDIR /src

# Copy over package.json
ADD package.json ./

# Install dependencies
RUN npm install --silent

# Copy over remaining sources
ADD . ./

# Start the api server
CMD [ "node", "server.js" ]

# The port(s) the api uses
EXPOSE 5001
