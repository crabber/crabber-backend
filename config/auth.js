module.exports = function(app) {

	'use strict';

	var Promise = require('bluebird');
	var bcrypt = require('bcrypt');
	var then = Promise.try;
	var userController = require('../app/controllers/UserController');
	var expressJwt = require('express-jwt');
	var jwt = require('jsonwebtoken');
	var fs = require('fs');
	var moment = require('moment');
    var log = require('loglevel');

	/**
	 *
	 * The following secret is only used for non production mode.
	 *
	 * In production mode we use asymetric cryptography (RSASSA using SHA-256 hash algorithm)
	 * with a public/private key pair which must be defined using the environment variables JWT_PUBLIC_KEY_FILE and JWT_PRIVATE_KEY_FILE.
	 *
	 */
	var secret = "no real secret";

	var privateKey, publicKey;

	if(process.env.NODE_ENV !== 'production') {
		log.warn('Not running in production mode. Cryptography is not safe.');
		app.use(expressJwt({ secret: process.env.JWT_SECRET || secret}).unless({path: ['/authenticate']}));
	} else {
		if(process.env.KEYS_BASE64_ENCODED) {
			log.debug('KEYS_BASE64_ENCODED is enabled. Decoding base64 encoded keys.');
			privateKey = new Buffer(process.env.JWT_PRIVATE_KEY, 'base64');
			publicKey = new Buffer(process.env.JWT_PUBLIC_KEY, 'base64');
		} else {
			privateKey = process.env.JWT_PRIVATE_KEY;
			publicKey = process.env.JWT_PUBLIC_KEY;
		}

		app.use(expressJwt({ secret: publicKey }).unless({path: ['/authenticate']}));
	}

	function verifyLogin(email, password, callback) {
		userController.findByEmail(email).then(function(user) {

			if (!user) {
				callback(null, false);
				return;
			} else {

				bcrypt.compare(password, user.password_hash, function(err, res) {
					if(!res || err) {
						callback(err, false);
						return;
					} else {
						callback(null, user);
						return;
					}
				});
			}

		}, function(error) {
			log.error('Failed to find user by email: ', error);

			if(error.name === 'ResourceNotFoundError') {
				callback(null, false);
				return;
			} else {
				callback(error, null);
				return;
			}
		});
	}

	app.post('/authenticate', function (req, res, next) {


		verifyLogin(req.body.username, req.body.password, function(error, user, message) {
			if (error) {
				res.status(500).send('Error occurred: ' + error);
				return;
			} else if(!user) {
				res.status(401).send('Incorrect username or password.');
			} else {

				// We are sending the profile inside the token
				var profile = { _id: user._id,
					email: user.email,
					full_name: user.full_name,
					groups: user.groups
				};

				var token;

				if(process.env.NODE_ENV !== 'production') {
					token = jwt.sign(profile, process.env.JWT_SECRET || secret);
				} else {
					token = jwt.sign(profile, privateKey, { algorithm: 'RS256'});
				}

				// TODO: check if jwt and moments have the same concept of "x minutes in the future".

				var session = {
					token: token,
					user: user._id,
					expires: {
						timestamp: moment().add(5, 'hours').toDate()
					},
					ipaddress: req.ip,
					client: req.body.client
				};

				log.debug(session);

				res.status(200).json({ token: token, profile: profile });
			}
		});
	});
};
