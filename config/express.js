var express = require('express'),
	path = require('path'),
	helmet = require('helmet'),
	bodyParser = require('body-parser'),
	cors = require('cors'),
	fs = require('fs');
	

/** 
 * 
 * The following secrets are only used for non production mode.
 * 
 * In production mode we use real (random) secrets from files defined
 * using the environment variable COOKIE_SECRET_FILE.
 * 
 */
var cookieSecret = "not a secret cookie secret";

/**
 * Express configuration
 */
module.exports = function(app) {
    'use strict';
    
	var env = app.get('env');
	
	var cookieSecretFile;

	// Logging
	if (env === 'development') {
		app.use(require('morgan')('dev'));
	} else if(env == 'production') {
        // Do not log any requests
    } else if (env !== 'test') {
		app.use(require('morgan')());
	}

	if(env !== 'production') {
		app.use(require('cookie-parser')(process.env.COOKIE_SECRET || cookieSecret));
	} else {
		app.use(require('cookie-parser')(process.env.COOKIE_SECRET));
	}

	app.use(bodyParser.json());
	app.use(require('connect-timeout')(10 * 1000));

	// Live reload
//	if (env === 'development') {
//		app.use(require('connect-livereload')());
//	}

	// Disable caching of scripts for easier testing
	if (env === 'development') {
		app.use(function noCache(req, res, next) {
			if (req.url.indexOf('/views/') === 0 || req.url.indexOf('/controllers/') === 0) {
				res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
				res.header('Pragma', 'no-cache');
				res.header('Expires', 0);
			}
			next();
		});
	}

	// Compression
	if (env === 'production') {
		app.use(require('compression')());
	}

	// Security
	app.use(helmet.xssFilter());
	app.use(helmet.nosniff());
	app.use(helmet.xframe('sameorigin'));
	app.use(helmet.hidePoweredBy());
	app.use(helmet.hsts({
		maxAge: 1000 * 60 * 60 * 24 // 1 day, increase later.
	}));
        
        app.use(cors());
	
	// Authentification
	require('./auth')(app);

	// Routing
	app.use(require('../app/routes'));

	// Static resources
	//app.use(express.favicon(path.join(__dirname, '../public', 'favicon.ico')));
	app.use(express.static(path.join(__dirname, '../public')));
	app.use(express.static(path.join(__dirname, '../bower_components')));

	// Error handler
	if (env === 'development') {
		app.use(require('errorhandler')());
	}
};
