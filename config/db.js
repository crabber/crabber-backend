'use strict';

var mongoose = require('mongoose');
var log = require('loglevel');

var mongoHost = process.env.MONGODB_HOST || process.env.MONGODB_PORT_27017_TCP_ADDR || 'localhost';
var mongoPort = process.env.MONGODB_PORT || process.env.MONGODB_PORT_27017_TCP_PORT || 27017;
var mongoDb = process.env.MONGODB_DB || 'crabber';

var mongoDbURL = process.env.MONGODB_URL || mongoHost + ':' + mongoPort + '/' + mongoDb;

if (mongoDbURL.indexOf('mongodb://') === -1) {
	mongoDbURL = 'mongodb://' + mongoDbURL;
}

module.exports.url = mongoDbURL;

// Connect to our database
log.info('Connect to mongodb: ' + mongoDbURL);
mongoose.connect(mongoDbURL);

// If the connection throws an error
mongoose.connection.on('error',function (err) {
	log.error('Mongoose connection error: ' + err + '\nMongo server started?');
});

// When the connection is disconnected
mongoose.connection.on('connected', function () {
	log.info('Mongoose connection connected');
});

// When the connection is connected
mongoose.connection.on('disconnected', function () {
	log.info('Mongoose connection disconnected');
});

module.exports.connection = mongoose.connection;
